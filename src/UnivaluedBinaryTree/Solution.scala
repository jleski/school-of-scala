package UnivaluedBinaryTree

/**
A binary tree is univalued if every node in the tree has the same value.
Return true if and only if the given tree is univalued.

Example 1:
Input: [1,1,1,1,1,null,1]
Output: true

Example 2:
Input: [2,2,2,5,2]
Output: false

Note:
The number of nodes in the given tree will be in the range [1, 100].
Each node's value will be an integer in the range [0, 99].
**/


/**
  * Definition for a binary tree node.
  * class TreeNode(var _value: Int) {
  *   var value: Int = _value
  *   var left: TreeNode = null
  *   var right: TreeNode = null
  * }
  */
object Solution {
  class TreeNode(var _value: Int) {
     var value: Int = _value
     var left: TreeNode = null
     var right: TreeNode = null
  }
  def dfs(root: TreeNode, value: Int): Boolean = {
    root == null || (root.value == value && dfs(root.left, value) && dfs(root.right, value))
  }
  def isUnivalTree(root: TreeNode): Boolean = {
    dfs(root, root.value)
  }
  def main(args: Array[String]): Unit = {
    val root =
    println(isUnivalTree(new TreeNode(1)))
  }

}
